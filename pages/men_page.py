from selenium.webdriver.remote import switch_to

from base.base_page import BasePage
from selenium.webdriver.common.by import By


class MenPage(BasePage):

    def __init__(self, driver):
        super().__init__(driver)
        self.curr_window = ""

    def find_men_link(self):
        men_link = self.find_element((By.XPATH, '//div[contains(text(), "Мужчины")]'))
        men_link.click()

    def sort_generally(self):
        sort_generally = self.find_element((By.XPATH, '//span[contains(text(), "По новизне")]'))
        self.driver.execute_script("arguments[0].click();", sort_generally)

    def choose_price_incr(self):
        price_incr = self.find_element((By.XPATH, '//p[contains(text(), "По возрастанию цены")]'))
        price_incr.click()

    def choose_price_interval(self):
        price_interval = self.is_clickable((By.XPATH, '//span[contains(text(), "Цена")]'))
        price_interval.click()

    def choose_price_interval_5(self):
        price_interval_5 = self.find_element((By.XPATH, '//p[contains(text(), "2999")]'))
        price_interval_5.click()

    def choose_price_interval_7(self):
        price_interval_7 = self.find_element((By.XPATH, '//p[contains(text(), "100000")]'))
        price_interval_7.click()

    def apply_price_filter(self):
        apply_button = self.find_element((By.XPATH, '//p[contains(text(), "Применить") and'
                                                    ' @data-qa="apply_productLabelType"]'))
        apply_button.click()

    def choose_region(self, locator: tuple = (By.XPATH, '//div/button[contains(text(), "Да, верно")]')):
        choose_region = self.find_element(locator)
        choose_region.click()

    def find_iframe_input(self):
        iframe = self.find_element((By.XPATH, "//form/input[@name='email']"))
        iframe.send_keys("warmane.suvar@gmail.com")

    def switch_to_iframe(self):
        iframe = self.find_element((By.XPATH, "//iframe[@id='fl-634793']"))
        self.driver.switch_to.frame(iframe)

    def close_iframe(self):
        iframe = self.find_element((By.XPATH, "//button[@title='Закрыть']"))
        iframe.click()

    def switch_to_default_content(self):
        self.driver.switch_to.default_content()
