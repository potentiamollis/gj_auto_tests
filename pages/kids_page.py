from base.base_page import BasePage
from selenium.webdriver.common.by import By
from typing import List
from selenium.webdriver.remote.webelement import WebElement


class KidsPage(BasePage):

    def find_kids_link(self):
        men_link = self.find_element((By.XPATH, '//div[contains(text(), "Дети")]'))
        men_link.click()

    def choose_region(self, locator: tuple = (By.XPATH, '//div/button[contains(text(), "Да, верно")]')):
        """
        Метод, позволяющий выбрать регион
        :param locator: локатор окна выбора региона
        :return:
        """
        choose_region = self.find_element(locator)
        choose_region.click()

    def find_iframe_input(self):
        """
        Метод, заполняющий email в iframe
        :return:
        """
        iframe = self.find_element((By.XPATH, "//form/input[@name='email']"))
        iframe.send_keys("warmane.suvar@gmail.com")

    def switch_to_iframe(self):
        iframe = self.find_element((By.XPATH, "//iframe[@id='fl-634793']"))
        self.driver.switch_to.frame(iframe)

    def close_iframe(self):
        """
        Метод, закрывающий iframe
        :return:
        """
        iframe = self.find_element((By.XPATH, "//button[@title='Закрыть']"))
        iframe.click()

    def switch_to_default_content(self):
        self.driver.switch_to.default_content()

    def find_clothes_list(self):
        clothes_list = self.is_elements_presence((By.XPATH, "//div[@data-wish-list='false']"))

    def find_elements(self) -> List[WebElement]:
        return self.driver.find_elements(By.XPATH, "//div[@data-wish-list='false']")

    def add_to_like(self):
        """
        Метод, реализующий добавление товара в избранное
        :return:
        """
        my_list = self.find_elements()
        my_list[2].click()

    def choose_size_104(self):
        chosen_cloth = self.is_clickable((By.XPATH, '//div[contains(text(), "104")]'))
        chosen_cloth.click()

    def go_to_wishlist(self):
        """
        Метод, реализующий переход в список избранных товаров
        :return:
        """
        wishlist_page = self.find_element((By.XPATH, '//a[contains(text(), "Перейти в избранное")]'))
        wishlist_page.click()

    def check_wishlist_count(self) -> int:
        return len(self.driver.find_elements(By.XPATH, "//a/img"))
