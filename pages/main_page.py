from selenium.webdriver.common.by import By
from base.base_page import BasePage


class MainPage(BasePage):

    def enter_to_lk(self):
        enter_to_lk = self.is_visible((By.XPATH, '//span[contains(text(), "Войти")]'))
        enter_to_lk.click()

    def input_login(self):
        input_login = self.find_element((By.XPATH, '//input[@name="email" and @data-qa="loginEmail"]'))
        input_login.send_keys("warmane.suvar@gmail.com")

    def input_password(self):
        input_password = self.find_element((By.XPATH, '//input[@name="password" and @data-qa="loginPassword"]'))
        input_password.send_keys("Aspire2022N")

    def submit_enter(self):
        submit_button = self.find_element((By.XPATH, "//button[@data-qa='loginSubmit']"))
        submit_button.click()

    def choose_region(self, locator: tuple = (By.XPATH, '//div/button[contains(text(), "Да, верно")]')):
        choose_region = self.find_element(locator)
        choose_region.click()


