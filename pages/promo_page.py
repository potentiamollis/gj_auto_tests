from base.base_page import BasePage
from selenium.webdriver.common.by import By


class PromoPage(BasePage):

    def __init__(self, driver):
        super().__init__(driver)
        self.list_to_compare = []
        self.number_to_compare = 120

    def go_to_promo_page(self):
        promo_link = self.find_element((By.XPATH, "//a[@href='https://www.gloria-jeans.ru/promotions']"))
        promo_link.click()

    def get_promo_list(self):
        self.list_to_compare = self.is_elements_presence((By.XPATH, '//div[@class="promotions-page__list '
                                                             'js-promotions-list"]/*'))

    def choose_region(self, locator: tuple = (By.XPATH, '//div/button[contains(text(), "Да, верно")]')):
        choose_region = self.find_element(locator)
        choose_region.click()
