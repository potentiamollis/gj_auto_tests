from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from data.constant import REF_TO_MAIN_PAGE
from selenium.webdriver.remote.webelement import WebElement


class BasePage:
    """
    Базовый класс, содержащий общие методы для всех тестов

    Arguments:
        driver (WebElement): экземпляр браузера Chrome
    Attributes:
        -
    """

    def __init__(self, driver):
        self.driver = driver
        self.ref = REF_TO_MAIN_PAGE
        self.wait = WebDriverWait(self.driver, 10)

    def find_element(self, locator: tuple) -> WebElement:
        """

        :param locator: локатор искомого элемента
        :type locator: tuple
        :return: WebElement
        """
        return self.wait.until(EC.visibility_of_element_located(locator), message=f"Can't find element"
                                                                            f" by locator {locator}")

    def is_elements_presence(self, locator: tuple) -> WebElement:
        """

        :param locator: локатор искомого элемента
        :type locator: tuple
        :return: WebElement
        """
        return self.wait.until(EC.presence_of_all_elements_located(locator), message=f"Can't find elementS"
                                                                                    f" by locator {locator}")

    def is_visible(self, locator: tuple) -> WebElement:
        return self.wait.until(EC.visibility_of_element_located(locator), message=f"ElementS with locator"
                                                                                  f" {locator} is still invisible")

    def is_clickable(self, locator: tuple) -> WebElement:
        return self.wait.until(EC.element_to_be_clickable(locator), message=f"ElementS with locator"
                                                                                  f" {locator} is still unclickable")

    def go_to_link(self):
        """
        Метод для перехода на главную страницу сайта
        :return:
        """
        return self.driver.get(self.ref)

    def assert_counting(self, list_to_compare: list, number_to_compare: int) -> None:
        """
        Метод, сравнивающий длину списка (например, количества товаров в корзине) с ожидаемым значением
        :param list_to_compare: список товаров
        :type list_to_compare: list
        :param number_to_compare: ожидаемое количество товаров
        :type number_to_compare: int
        :return:
        """
        assert len(list_to_compare) == number_to_compare, f"Error! Lenght of list {len(list_to_compare)} " \
                                                          f"of list not equal {number_to_compare}"

