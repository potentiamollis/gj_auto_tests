import pytest
from selenium import webdriver
from selenium.webdriver.chrome.options import Options as chrome_options


def pytest_addoption(parser):
    parser.addoption("--browser-name", default="chrome", action="store", help="Choose browser"
                                                                              " - chrome or firefox")

@pytest.fixture(scope="class")
def browser(get_chrome_options):
    options = get_chrome_options
    #browser_name = request.config.getoption("browser_name")
    browser = webdriver.Chrome(options=options)
    yield browser
    browser.quit()


@pytest.fixture(scope="class")
def get_chrome_options():
    options = chrome_options()
    options.add_argument("chrome")  # or headless
    options.add_argument("--start-maximized")
    options.add_argument("--window-size=2880,1800")
    return options
