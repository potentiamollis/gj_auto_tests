import pytest
from pages.main_page import MainPage


@pytest.mark.usefixtures("browser")
def test_login_link(browser):
    main_page = MainPage(browser)
    main_page.go_to_link()
    main_page.choose_region()
    main_page.enter_to_lk()
    main_page.input_login()
    main_page.input_password()
    main_page.submit_enter()
