from pages.promo_page import PromoPage

# @pytest.mark.regression
# @pytest.mark.xfail
# @pytest.mark.parametrize("language", ["ru", "en-gb"])


def test_check_promotions(browser):
    promo_page = PromoPage(browser)
    promo_page.go_to_link()
    promo_page.choose_region()
    promo_page.go_to_promo_page()
    promo_page.get_promo_list()
    promo_page.assert_counting(promo_page.list_to_compare, promo_page.number_to_compare)
