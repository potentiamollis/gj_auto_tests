from pages.kids_page import KidsPage
import time


def test_kids_shopping(browser):

    kids_page = KidsPage(browser)
    kids_page.go_to_link()
    kids_page.choose_region()
    kids_page.find_kids_link()
    kids_page.switch_to_iframe()
    time.sleep(7)
    kids_page.close_iframe()
    kids_page.switch_to_default_content()
    time.sleep(15)
    kids_page.add_to_like()
    kids_page.choose_size_104()
    kids_page.go_to_wishlist()

    time.sleep(15)
    wishlist_counter = kids_page.check_wishlist_count()
    assert 1 == wishlist_counter, f"Error! Count of wishlist {wishlist_counter} not equal 1"

    time.sleep(10)
