from pages.men_page import MenPage
import pytest
import time


@pytest.mark.xfail
def test_filer_men_goods(browser):
    men_page = MenPage(browser)
    men_page.go_to_link()
    men_page.choose_region()
    men_page.find_men_link()
    men_page.switch_to_iframe()
    men_page.close_iframe()
    men_page.switch_to_default_content()
    men_page.sort_generally()
    men_page.choose_price_incr()
    men_page.choose_price_interval()
    men_page.choose_price_interval_5()
    men_page.choose_price_interval_7()
    men_page.apply_price_filter()

    time.sleep(10)
